# Example deploying to AWS Elastic Container Service
## How to use this repo

1. To start you need to fork a repo.
2. You'll also need and AWS account with an [ECS service](https://aws.amazon.com/ecs/) set up and running.
3. You will need a dockerhub account.
4. An AWS access key with permissions associated to execute the RegisterTaskDefinition and UpdateService actions.
5. Enable Pipelines in your repo and configure the required [repository variables](https://confluence.atlassian.com/bitbucket/variables-in-pipelines-794502608.html#Variablesinpipelines-Repositoryvariables)
You will need the following variables configured for this example:
- AWS_DEFAULT_REGION: The AWS region you are deploying your ECS task to.
- AWS_ACCESS_KEY_ID: Access key id of the IAM user that will perform the ECS deployment.
- AWS_SECRET_ACCESS_KEY: Access key of the IAM user that will perform the ECS deployment.
- DOCKERHUB_USERNAME: Username to log in to Dockerhub.
- DOCKERHUB_PASSWORD: Password to log in to Dockerhub.
6. You should have an existing ECS cluster, service and task definition in ECS.
